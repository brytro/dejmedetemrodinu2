module.exports = {
  mode: 'jit',
  purge: {
    //enabled: false,
    layers: ['components', 'utilities'],
    content: [
      './**/*.html',
      './*.html',
    ],
    safelist: [
    ]
  },

  theme: {

    fontFamily: {
      sans: ['Roboto', 'sans-serif'],
      serif: ['Roboto', 'serif'],
      heading: ['Roboto', 'sans-serif'],
      body: ['Roboto', 'sans-serif'],
    },

    container: {
      center: true,
      screens: {
        'sm': "100%",
        'md': "640px",
        'lg': "1024px",
        'xl': "1280px",
        '2xl': "1280px",
      },
      padding: {
        DEFAULT: '1rem',
        md: '1rem',
      },
    },

    extend: {

        backgroundImage: theme => ({
            'pattern': "url('../../../img/bg-small.png')",
            'circles': "url('../../../img/linka.png')",
            'akva3': "url('../../../img/akva3_large.jpg')",
        }),

        colors: {
            mypurple: {
                'light': "#C1007B",
                DEFAULT: "#8A0057",
                'dark': "#650040",
            },
            myblue: {
                'light': "#3D92CF",
                DEFAULT: "#182F82",
                'dark': "#0C1947",
            },
            mygray: {
                'light': "#F5F4F2",
                DEFAULT: "#E0DFE5",
                'dark': "#CFCED2",
            },
            bodycolor: {
                DEFAULT: "#222",
            },
            headingcolor: {
                DEFAULT: "#182F82",
            },
        },

        screens: {
            'print': {'raw': 'print'},
        },

    },
  },

  darkMode: false,

  plugins: [
  ],
}
