</div>

<footer class="relative bg-white py-20">
    <div class="container">
        <div class="grid grid-cols-1 md:grid-cols-3 lg:grid-cols-6 gap-10 md:gap-6 mb-12 text-sm md:text-base">

            <?php if ( have_rows( 'menu_v_paticce', 'option' ) ) : ?>
                <?php while ( have_rows( 'menu_v_paticce', 'option' ) ) : the_row(); ?>

                    <div class="">
                        <h5 class="h5 !mb-6"><?php the_sub_field( 'nazev' ); ?></h5>

                        <ul class="flex flex-col gap-3">

                            <?php if ( have_rows( 'polozky' ) ) : ?>
                                <?php while ( have_rows( 'polozky' ) ) : the_row(); ?>

                                    <?php $polozka = get_sub_field( 'polozka' ); ?>
                                    <?php if ( $polozka ) { ?>

                                        <li>
                                            <a class="" href="<?php echo $polozka['url']; ?>" target="<?php echo $polozka['target']; ?>"><?php echo $polozka['title']; ?></a>
                                        </li>

                                    <?php } ?>

                                <?php endwhile; ?>

                            <?php else : ?>
                                <?php // no rows found ?>
                            <?php endif; ?>

                        </ul>

                    </div>

                <?php endwhile; ?>
            <?php else : ?>
                <?php // no rows found ?>
            <?php endif; ?>

        </div>

        <div class="">
            <p class="text-xs">&copy; Copyright <?php echo date("Y") ?> <?php echo get_bloginfo( 'name' ); ?></p>
        </div>

    </div>
</footer>

<?php wp_footer(); ?>

<?php the_field( 'scripty_v_paticce', 'option' ); ?>

</body>
</html>
