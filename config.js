module.exports = {
	config: {
		tailwindjs: "./tailwind.config.js",
        proxy: "https://blueprint.local",
	},
	paths: {
		root: "./",
		src: {
			base: "./src",
			css: "./src/css",
			js: "./src/js",
			img: "./src/img"
		},
		dist: {
			base: "./dist",
			css: "./dist/css",
			js: "./dist/js",
			img: "./dist/img"
		}
	}
}