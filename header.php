<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="Brýtro.cz - https://www.brytro.cz">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="format-detection" content="telephone=no" />
	
	<?php wp_head(); ?>

	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap" rel="stylesheet">

	<script>
		document.documentElement.className = document.documentElement.className.replace("no-js", "js");
	</script>

	<?php the_field( 'scripty_v_hlavicce', 'option' ); ?>

</head>
<body <?php body_class(); ?>>

	<?php the_field( 'scripty_na_zacatku_body', 'option' ); ?>

	<?php echo get_template_part('template-parts/parts/navigation', null, array()); ?>

	<div class="main">